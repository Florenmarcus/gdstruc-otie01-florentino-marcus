package com.company;
import java.util.Random;


public class Main
{
    public static void main(String[] args)
    {
        // Deck of cards
        CardStack stack = new CardStack();
        //Player Hand
        CardStack playerHand = new CardStack();
        //Discard Pile
        CardStack discardedPile = new CardStack();
        // Random Generator
        Random possibilities = new Random();

        // Deck will be filled with Cards
        for(int i = 0; i < 6; i++)
        {
            stack.push(new Cards("Clover"));
            stack.push(new Cards("Spades"));
            stack.push(new Cards("Hearts"));
            stack.push(new Cards("Diamonds"));
            stack.push(new Cards("Joker"));
        }

        //Main
        boolean emptyCheck = false;
        int turn = 1;
        while(!emptyCheck)
        {
            System.out.println("===============================================================");
            System.out.println("Turn: " + turn);
            turn++;
            outcomes(stack, playerHand, discardedPile, possibilities);
            cardsCheck(stack, playerHand, discardedPile);
            System.out.println("===============================================================");
            System.out.println("Press Any Key to Continue...");
            new java.util.Scanner(System.in).nextLine();

            if (stack.isEmpty())
            {
                emptyCheck = true;
                System.out.println("\n===============================================================");
                System.out.println("Deck is already empty! GG.");
                System.out.println("===============================================================");
            }

        }
    }

    public static void deckDraw(CardStack stack, CardStack playerHand, Random possibilities)
    {
        int outcome ;
        outcome = 1 + possibilities.nextInt(5);

        System.out.println("Command: Draw Card/s from the deck activated!");

        int count = stack.count();
        if (count < outcome)
        {
            outcome = count;
        }

        for(int i = 0; i < outcome; i++)
        {
            System.out.println("Drawn Card: " + stack.peek());
            Cards temporary = stack.peek();
            playerHand.push(temporary);
            stack.pop();
        }
    }

    public static void discard(CardStack playerHand, CardStack discardedPile, Random possibilities)
    {
        System.out.println("Command: Discard Card/s activated!");

        if(playerHand.isEmpty())
        {
            System.out.println("Player's Hand is Empty. You discarded no cards!");
        }
        else
        {
            int outcome;
            outcome = 1 + possibilities.nextInt(5);

            int count = playerHand.count();
            if (count < outcome)
            {
                outcome = count;
            }

            for(int i = 0; i < outcome; i++)
            {
                System.out.println("Discarding : " + playerHand.peek());
                Cards temporary = playerHand.peek();
                discardedPile.push(temporary);
                playerHand.pop();
            }
        }
    }

    public static void discardDraw(CardStack playerHand, CardStack discardedPile, Random possibilities)
    {
        System.out.println("Command: Draw Card/s from the discarded pile activated!");

        if(discardedPile.isEmpty())
        {
            System.out.println("The discard Pile is Empty. You drew no cards!");
        }
        else
        {
            int outcome;
            outcome = 1 + possibilities.nextInt(5);

            int count = discardedPile.count();
            if (count < outcome)
            {
                outcome = count;
            }

            for(int i = 0; i < outcome; i++)
            {
                System.out.println("Drawn Card : " + discardedPile.peek());
                Cards temporary = discardedPile.peek();
                playerHand.push(temporary);
                discardedPile.pop();
            }
        }
    }

    public static void outcomes(CardStack stack, CardStack playerHand, CardStack discardedPile, Random possibilities)
    {
        int outcome;
        outcome = 1 + possibilities.nextInt(3);

        if (outcome == 1)
        {
            deckDraw(stack, playerHand, possibilities);
        }
        else if(outcome == 2)
        {
            discard(playerHand, discardedPile, possibilities);
        }
        else
        {
            discardDraw(playerHand, discardedPile, possibilities);
        }
    }

    public static void cardsCheck(CardStack stack, CardStack playerHand, CardStack discardedPile)
    {
        System.out.println("\nChecking: ");

        //List of cards that the player is currently holding
        System.out.println("Player's Hand: ");
        int count = playerHand.count();
        if (count == 0)
        {
            System.out.println("Empty");
        }
        else
        {
            playerHand.printStack();
        }

        //Number of remaining cards in the player deck
        System.out.print("Remaining Cards in the Player's deck:");
        System.out.println(stack.count());


        //Number of cards in the discarded pile
        System.out.print("Number of Cards in the Discard Pile:");
        System.out.println(discardedPile.count());
    }
}
