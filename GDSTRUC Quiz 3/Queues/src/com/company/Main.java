package com.company;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        ArrayQueue queue = new ArrayQueue(15);
        Random possibilities = new Random();

        int game = 1;

        while(game != 11)
        {
            System.out.println("\nRequired Players to start a game: 5");
            // Queue up players
            queuePlayers(queue, possibilities);
            while (queue.size() < 5)
            {
                queuePlayers(queue, possibilities);
            }


            //Players in the Queue
            if(queue.size() >= 5)
            {
                System.out.println("\nGame: " + game);
                System.out.println("===============================================================");
                System.out.println("There are " + queue.size() + " players in the queue.");
                System.out.println("Players in queue: ");
                queue.printQueue();


                // Removing 5 players in the queue
                System.out.println("\nRemoving 5 Players from the queue: ");
                for(int i = 0; i < 5; i++)
                {
                    System.out.println(queue.dequeue());
                }


                System.out.println("\nGame Starts! ... Game Ended!");
                System.out.println("Players remaining in queue: " + queue.size());
                queue.printQueue();
                System.out.println("===============================================================");
                game++;
                System.out.println("Press Any Key to Continue...");
                new java.util.Scanner(System.in).nextLine();
            }
        }
        System.out.println("10 games have been successfully made!");
    }

    public static void queuePlayers(ArrayQueue queue, Random possibilities)
    {
        int outcome ;
        outcome = 1 + possibilities.nextInt(7);

        System.out.println("NOTE: " + outcome + " players have been placed in the queue");
        for(int i = 0; i < outcome; i++)
        {
            System.out.print("Input a Name for the Player: ");
            Scanner scanner = new Scanner(System.in);
            String name = scanner.nextLine();
            queue.enqueue(new Player(name));
        }
    }
}
