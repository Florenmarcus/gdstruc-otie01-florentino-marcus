package com.company;

import java.util.Objects;

public class Player {
    private String Name;

    public Player(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "Player{" +
                "Name='" + Name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(Name, player.Name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Name);
    }
}
