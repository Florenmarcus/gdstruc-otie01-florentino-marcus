package com.company;

public class Main {

    public static void main(String[] args) {

        Player ploo = new Player("Plooful");
        Player wardell = new Player("TSM Wardell");
        Player deadlyJimmy = new Player("DeadlyJimmy");
        Player subroza = new Player("Subroza");
        Player annieDro = new Player("C9 Annie");

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(ploo.getName(), ploo);
        hashtable.put(wardell.getName(), wardell);
        hashtable.put(deadlyJimmy.getName(), deadlyJimmy);
        hashtable.put(subroza.getName(), subroza);
        hashtable.put(annieDro.getName(), annieDro);

        hashtable.printHashtable();

        System.out.println("\nGet() function...");
        System.out.println(hashtable.get("Plooful"));

        System.out.println("\nRemove() function...");
        System.out.println(hashtable.remove("Plooful"));

        hashtable.printHashtable();

        System.out.println("\nDouble check if search is null...");
        System.out.println(hashtable.get("Plooful"));
    }
}
