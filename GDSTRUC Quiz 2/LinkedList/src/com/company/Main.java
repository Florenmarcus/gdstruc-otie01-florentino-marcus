package com.company;

public class Main {

    public static void main(String[] args) {
        Player asuna = new Player(1, "Asuna", 100);
        Player kirito = new Player(2, "Kirito", 100);
        Player yui = new Player(3, "Yui", 0);
        Player heathcliff = new Player(0, "Heathcliff", 999);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList(asuna);
        playerLinkedList.addToFront(kirito);
        playerLinkedList.addToFront(yui);
        playerLinkedList.addToFront(heathcliff);

        // Current List *Already containing the Size counter
        System.out.println("Current List: ");
        playerLinkedList.printList();
        playerLinkedList.sizeCounter();

       // Removes the first Element *Already containing the Size counter
       System.out.println("Updated List (Remove First Element): ");
       playerLinkedList.removeElement();
       playerLinkedList. printList();
       playerLinkedList.sizeCounter();

        //contains()
        System.out.println("contains() ");
        System.out.println(playerLinkedList.containsCheck(heathcliff));

        //indexOf()
        System.out.println("\nindexOf()");
        System.out.print("Index: ");
        System.out.print(playerLinkedList.indexOfCheck(asuna));
    }
}
