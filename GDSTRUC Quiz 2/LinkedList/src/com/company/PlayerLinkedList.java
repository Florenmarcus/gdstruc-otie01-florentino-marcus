package com.company;

import java.util.Iterator;

public class PlayerLinkedList {
    private PlayerNode head;

    public PlayerLinkedList(Player head) {
        this.head = new PlayerNode(head);
    }

    public void addToFront(Player player)
    {
        PlayerNode current = new PlayerNode(player);
        current.setNextPlayer(head);
        head.setPreviousPlayer(current);
        head = current;
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while(current != null)
        {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public void removeElement()
    {
        head = head.getNextPlayer();
        head.setPreviousPlayer(null);
    }

    public void sizeCounter()
    {
        int size = 0;
        PlayerNode current = head;
        while(current != null)
        {
            current = current.getNextPlayer();
            size++;
        }
            System.out.print("Size = " + size + "\n\n");

    }

    public boolean containsCheck(Player search)
    {
        PlayerNode current = head;
        while (current != null)
        {
            if (current.getPlayer() == search)
            {
                System.out.print("Search = ");
                return true;
            }
            current = current.getNextPlayer();
        }
        System.out.print("Search = ");
        return false;
    }

    public int indexOfCheck(Player search)
    {
        int index = 0;
        PlayerNode current = head;
        while (current.getPlayer() != search)
        {
            current = current.getNextPlayer();
            index++;
        }
        return index;
    }
}
